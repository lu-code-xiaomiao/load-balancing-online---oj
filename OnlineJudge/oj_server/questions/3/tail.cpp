#ifndef COMPILER_ONLINE

#include "header.cpp"

#endif

void Test1() {
    vector < int > v = { -1, 0, 3, 5, 9, 12 };
    int target = Solution().search(v, 9);
    if (target == 4) {
        cout << "Test 1 ...OK!" << endl;
    } else {
        cout << "Test 1 ...Failed!" << endl;
    }
}

void Test2() {
    vector<int> v = { -1, 0, 3, 5, 9, 12 };
    int target = Solution().search(v, 2);
    if (target == -1) {
        cout << "Test 2 ...OK!" << endl;
    } else {
        cout << "Test 2 ...Failed!" << endl;
    }
}

int main() {
    Test1();
    Test2();
    return 0;
}
