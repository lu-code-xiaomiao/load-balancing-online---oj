#ifndef COMPILER_ONLINE

#include "header.cpp"

#endif

void Test1() {
    vector<int> v = {5, 3, 2, 1};
    vector<int> res = Solution().sortArray(v);
    if (res == v) {
        cout << "Test 1 ...OK!" << endl;
    } else {
        cout << "Test 1 ...Failed!" << endl;
    }
}

void Test2() {
    vector<int> v = {5, 1, 1, 2, 0, 0};
    vector<int> res = Solution().sortArray(v);
    if (res == v) {
        cout << "Test 2...OK!" << endl;
    } else {
        cout << "Test 2 ...Failed!" << endl;
    }
}

int main() {
    Test1();
    Test2();
    return 0;
}
