#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif 

void Test1()
{
    vector<int> v = {1, 2, 3, 4, 5, 6};
    int max = Solution().Max(v);
    if(max == 6){
        cout << "Test 1 ...OK!" << endl;
    }else{
        cout << "Test 1 ...Failed!" << endl;
    }
}

void Test2()
{
    vector<int> v = {-1, -2, -3, -4, -5, -6};
    int max = Solution().Max(v);
    if(max == -1){ 
        cout << "Test 2 ...OK!" << endl;
    }else{
        cout << "Test 2 ...Failed!" << endl;
    }
}
int main()
{
    Test1();
    Test2();
    return 0;
}
