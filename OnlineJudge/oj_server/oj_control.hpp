#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <cassert>
#include <fstream>
#include <algorithm>
#include <jsoncpp/json/json.h>

#include "../comm/httplib.h"
#include "../comm/util.hpp"
#include "../comm/log.hpp"
#include "oj_model.hpp"
#include "oj_view.hpp"

namespace ns_control {
    using namespace std;
    using namespace ns_log;
    using namespace ns_util;
    using namespace ns_model;
    using namespace ns_view;
    using namespace httplib;

    //提供服务的主机
    class Machine {
    public:
        std::string ip; //编译服务的ip
        int port;       //编译服务的port
        uint64_t load;  //编译服务的负载数量
        std::mutex *mtx;//C++中mutex是禁止拷贝的，所有使用指针来完成
    public:
        Machine() : ip(""), port(0), load(0), mtx(nullptr) {}

        ~Machine() {}

    public:

        //递增负载
        void IncLoad() {
            if (mtx) mtx->lock();
            ++load;
            if (mtx) mtx->unlock();
        }

        //递减负载
        void DecLoad() {
            if (mtx) mtx->lock();
            --load;
            if (mtx) mtx->unlock();
        }

        // 清除负载
        void ResetLoad(){
            if (mtx) mtx->lock();
            load = 0;
            if (mtx) mtx->unlock();
        }

        //获取主机负载
        uint64_t Load() {
            uint64_t _load = 0;
            if (mtx) mtx->lock();
            _load = load;
            if (mtx) mtx->unlock();
            return _load;
        }
    };

    const std::string service_machine = "./conf/service_machine.conf";
    
    //负载均衡模块
    class LoadBlance {
    private:
        // 可以提供给我们服务编译的所有主机，每一台主机都有自己的下标，充当当前主机的id
        std::vector<Machine> machines;
        // 所有在线主机id
        std::vector<int> online;
        // 所有离线主机id
        std::vector<int> offline;
        //保证LoadBlance它的数据安全
        std::mutex mtx;
    public:
        LoadBlance() {
            assert(LoadConf(service_machine));
            LOG(INFO) << "加载 " << service_machine << " 成功" << "\n";
        }

        ~LoadBlance() {}

    public:
        // 加载主机
        bool LoadConf(const std::string &machine_conf) {
            std::ifstream in(machine_conf);
            if (!in.is_open()) {
                LOG(FATAL) << " 加载: " << machine_conf << " 失败" << "\n";
                return false;
            }
            std::string line;
            while (std::getline(in, line)) {
                std::vector<std::string> tokens;
                StringUtil::SplitString(line, &tokens, ":");
                if (tokens.size() != 2) {
                    LOG(WARNING) << " 切分 " << line << " 失败" << "\n";
                    continue;
                }
                Machine m;
                m.ip = tokens[0];
                m.port = atoi(tokens[1].c_str());
                m.load = 0;
                m.mtx = new std::mutex();

                online.push_back(machines.size());
                machines.push_back(m);
            }

            in.close();
            return true;
        }

        // 智能选择
        bool SmartChoice(int *id, Machine **m) {
            //1.使用选择好的主机（更新该主机的负载）
            //2.我们需要可能离线该主机
            mtx.lock();
            //负载均衡的算法：
            //1.随机数算法；2.轮询+随机算法
            int online_num = online.size();
            if (online_num == 0) {
                mtx.unlock();
                LOG(WARNING) << " 所有的后端编译主机已经全部离线，请运维的同事尽快查看" << "\n";
                return false;
            }
            //通过遍历的方式，找到所有负载最小的机器
            *id = online[0];
            *m = &machines[online[0]];
            uint64_t min_load = machines[online[0]].Load();
            for (int i = 1; i < online_num; i++) {
                uint64_t curr_load = machines[online[i]].Load();
                if (min_load > curr_load) {
                    min_load = curr_load;
                    *id = online[i];
                    *m = &machines[online[i]];
                }
            }
            mtx.unlock();
            return true;
        }

        // 离线主机
        void OfflineMachine(int which) {
            mtx.lock();
            for (auto iter = online.begin(); iter != online.end(); iter++) {
                if (*iter == which) {
                    machines[which].ResetLoad();//负载清0
                    //要离线的主机找到了
                    online.erase(iter);
                    offline.push_back(which);
                    break;//因为break的存在，所以我们暂时不考虑迭代器失效的问题
                }
            }
            mtx.unlock();
        }

        // 在线主机
        void OnlineMachine() {
            //当所有主机都离线的时候，我们统一上线
            mtx.lock();
            online.insert(online.end(), offline.begin(), offline.end());
            offline.erase(offline.begin(), offline.end());
            mtx.unlock();
            LOG(INFO) << "所有的离线主机已上线......" << "\n";
        }

        //for test
        void ShowMachine() {
            mtx.lock();
            std::cout << "当前在线主机列表：";
            for (auto id: online) {
                std::cout << id << " ";
            }
            std::cout << std::endl;
            std::cout << "当前离线主机列表：";
            for (auto id: offline) {
                std::cout << id << " ";
            }
            std::cout << std::endl;
            mtx.unlock();
        }
    };

    //这是我么核心业务逻辑的控制器
    class Control {
    private:
        Model model_;            //model_：主要用来和数据进行交互，对外提供访问数据接口
        View view_;              //提供html渲染功能
        LoadBlance load_blance_; //核心负载均衡器
    public:
        Control() {}

        ~Control() {}

    public:
        // 恢复离线主机上线
        void RecoveryMachine(){
            load_blance_.OnlineMachine();
        }
        // 根据全部题目数据构建网页
        bool AllQuestions(string *html) {
            bool ret = true;
            vector<Question> all;
            if (model_.GetAllQuestions(&all)) {
                // 1. 先对题号进行排序
                sort(all.begin(), all.end(), [](const Question &q1, const Question &q2){
                    return atoi(q1.number.c_str()) < atoi(q2.number.c_str());//升序排序
                });
                // 2. 获取题目信息成功，将所有的题目数据构建成网页
                view_.AllExpandHtmml(all, html);
            } else {
                *html = "获取题目失败， 形成题目列表失败";
                ret = false;
            }
            return ret;
        }

        // 根据一道题目数据构建网页
        bool OneQuestion(const string &number, string *html) {
            bool ret = true;
            Question q;
            if (model_.GetOneQuestion(number, &q)) {
                // 获取指定题目信息成功，将指定题目数据构建成网页
                view_.OneExpandHtmml(q, html);
            } else {
                *html = "指定题目：" + number + " 不存在！";
                ret = false;
            }
            return ret;
        }

        //in_json包含：
        //code：#include...
        //input：“”
        void Judge(const std::string &number, const std::string in_json, std::string *out_json) {
            //LOG(DEBUG) << in_json << "\nnumnber:" << number << "\n";
            //1.根据题目编号拿到题目细节
            Question q;
            model_.GetOneQuestion(number, &q);

            //2.对in_json进行反序列化，得到题目id和用户提交的源代码
            Json::Reader reader;
            Json::Value in_value;
            reader.parse(in_json, in_value);
            std::string code = in_value["code"].asString();

            //3.重新拼接用户代码+测试用例代码，形成新的代码
            Json::Value compile_value;
            compile_value["input"] = in_value["input"].asString();
            compile_value["code"] = code + "\n" + q.tail;//这里加\n是为了展示的代码和测试用例代码，在编译的时候，不发生错误（能够去掉那个#ifdefine）
            compile_value["cpu_limit"] = q.cpu_limit;
            compile_value["mem_limit"] = q.mem_limit;
            Json::FastWriter writer;
            std::string compile_string = writer.write(compile_value);

            //4.选择负载最低的主机
            //规则：一直选择，直到主机可用，否则，就是全部挂掉
            while (true) {
                int id = 0;
                Machine *m = nullptr;
                if (!load_blance_.SmartChoice(&id, &m)) {
                    break;
                }
                //5.然后发起http请求，得到结果
                Client cli(m->ip, m->port);
                m->IncLoad();
                LOG(INFO) << " 选择主机成功，主机id：" << id << " 详情：" << m->ip << ":" << m->port << "当前主机的负载是：" << m->Load() << "\n";
                if (auto res = cli.Post("/compile_and_run", compile_string, "application/json;charset=utf-8")) {
                    //6.将结果赋值给out_json
                    if (res->status == 200) {
                        *out_json = res->body;
                        m->DecLoad();
                        LOG(INFO) << " 请求编译和运行服务成功......" << "\n";
                        break;
                    }
                    m->DecLoad();
                } else {
                    //请求失败
                    LOG(ERROR) << " 选择当前请求的主机的id：" << id << " 详情：" << m->ip << ":" << m->port << " 可能已经离线"
                               << "\n";
                    load_blance_.OfflineMachine(id);
                    load_blance_.ShowMachine();//仅仅是为了调式
                }
            }
        }
    };
}


