#include <iostream>
#include <string>
#include <ctemplate/template.h>

int main()
{
    std::string in_html = "./test.html";//要处理的网页信息
    std::string value = "hello";
    //形成数据字典
    ctemplate::TemplateDictionary root("test");//相当于定义了一个 unordered_map<> test;
    root.SetValue("key", value); //可以理解为test.insert({});

    //获取被渲染网页对象
    ctemplate::Template *tpl = ctemplate::Template::GetTemplate(in_html, ctemplate::DO_NOT_STRIP);

    //添加数据字典到网页中
    std::string out_html;
    tpl->Expand(&out_html, &root);

    //完成了渲染
    std::cout << out_html << std::endl;
}
