/*该文件仅仅用于测试，和项目中的其他文件没有任何关系*/

#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <signal.h>

void handler(int signo){
    std::cout << "signo: " << signo << std::endl;
}
int main()
{

    // 资源不足，导致OS终止进程，是通过信号终止的
    for(int i = 0; i < 31; i++){
        signal(i, handler);  
    }
    // 限制累计运行时长
    struct rlimit r;
    r.rlim_cur = 1;
    r.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_CPU, &r);
    while(1);
    
    // 限制内存
    //struct rlimit r;
    //r.rlim_cur = 1024 * 1024 * 20;//20M
    //r.rlim_max = RLIM_INFINITY;
    //setrlimit(RLIMIT_AS, &r);
    //int count = 0;
    //while(true){
    //    int* p = new int[1024*1024];
    //    count++;
    //    std::cout << "size: " << count << std::endl;
    //    sleep(1);
    //}
    return 0;
}
