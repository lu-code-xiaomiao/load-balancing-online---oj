#pragma once

#include "compiler.hpp"
#include "runner.hpp"
#include "../comm/log.hpp"
#include "../comm/util.hpp"
#include <signal.h>
#include <jsoncpp/json/json.h>

namespace ns_compile_and_run {
    using namespace ns_log;
    using namespace ns_util;
    using namespace ns_runner;
    using namespace ns_compiler;

    class CompileAndRun {
    public:
        // code > 0：进程收到了信号导致异常崩溃
        // code < 0：整个过程非运行报错（代码为空，编译报错等）
        // code = 0：整个过程全部完成
        // 将错误代码转为描述（CodeToDesc()）
        static std::string CodeToDesc(int code, const std::string &file_name) {
            std::string desc;
            switch (code) {
                case 0:
                    desc = "编译运行成功";
                    break;
                case -1:
                    desc = "提交的代码为空";
                    break;
                case -2:
                    desc = "未知错误";
                    break;
                case -3:
                    FileUtil::ReadFile(PathUtil::CompilerError(file_name), &desc, true);
                    break;
                case SIGABRT: //6号信号
                    desc = "内存超过了范围";
                    break;
                case SIGXCPU: //24号信号
                    desc = "CPU使用超时";
                    break;
                case SIGFPE: //8号信号
                    desc = "浮点数溢出";
                    break;
                default :
                    desc = "未知" + std::to_string(code);
                    break;
            }
            return desc;
        }
        //清理temp目录下的临时文件
        static void RemoveTempFile(const std::string &file_name) {
            /*清理文件的个数是不确定的，但是有哪些我们是知道的*/
            //unlink函数：是Linux下删除特定文件的一个函数，参数是字符串形式
            //清理源文件
            std::string _src = PathUtil::Src(file_name);
            if (FileUtil::IsFileExists(_src)) unlink(_src.c_str());

            //清理编译出错文件
            std::string _compiler_error = PathUtil::CompilerError(file_name);
            if (FileUtil::IsFileExists(_compiler_error)) unlink(_compiler_error.c_str());

            //清理可执行程序文件
            std::string _execute = PathUtil::Exe(file_name);
            if (FileUtil::IsFileExists(_execute)) unlink(_execute.c_str());

            //清理标准输入文件
            std::string _stdin = PathUtil::Stdin(file_name);
            if (FileUtil::IsFileExists(_stdin)) unlink(_stdin.c_str());

            //清理标准输出文件
            std::string _stdout = PathUtil::Stdout(file_name);
            if (FileUtil::IsFileExists(_stdout)) unlink(_stdout.c_str());

            //清理标准错误文件
            std::string _stderr = PathUtil::Stderr(file_name);
            if (FileUtil::IsFileExists(_stderr)) unlink(_stderr.c_str());
        }

        /*
         * 输入：
         *      code：用户提交的代码
         *      input：用户给自己提交代码对应的输入，不做处理
         *      cpu_limit：时间要求
         *      mem_limit：空间要求
         *
         * 输出：
         * 必填字段：
         *      status：状态码
         *      reason：请求结果
         * 选填字段：
         *      stdout：我的程序运行完的结果
         *      stderr：我的程序运行完的错误结果
         * */
        /*
         * start函数功能:
         *      通过网络接收用户传过来的json串（in_json），其中in_json包含如下：
         *          in_json：
         *          {
         *              code: “#include <iostream> ....int main(){...}”,
         *              input: "用户的输入（像牛客哪些）",
         *              cpu_limit: "1024",
         *              mem_limit: "30"
         *          }
         *       我们start函数就需要去解析这个in_json串，将数据取出来；
         *       然后将提取出来的代码写入到特定的文件中，因为存在多个用户提交代码，我们就需要保证每个文件的唯一性；
         *
         * */
        static void Start(const std::string &in_json, std::string *out_json) {
            Json::Value in_value;
            Json::Reader reader;
            reader.parse(in_json, in_value);//in_json——你要解析是谁；in_value——解析的结果放到哪里

            std::string code = in_value["code"].asString();   //提取用户的代码
            std::string input = in_value["input"].asString(); //提取用户的输入

            int cpu_limit = in_value["cpu_limit"].asInt(); //提取cpu限制
            int mem_limit = in_value["mem_limit"].asInt(); //提取内存限制

            int status_code = 0;//状态码
            int run_result = 0;// 在goto之间定义的变量是不允许的，我们提前定义
            std::string file_name;// 需要内部形成的唯一文件名（为后续编译和运行提供好文件名）

            Json::Value out_value;
            if (code.size() == 0) {
                status_code = -1;// 表示代码为空
                goto END;
            }

            // 形成的文件名只具有唯一性，没有目录没有后缀
            // 毫秒级时间戳+原子性递增的唯一值：来保证唯一性
            file_name = FileUtil::UniqFileName();

            if (!FileUtil::WriteFile(PathUtil::Src(file_name), code)) // 形成临时源文件
            {
                status_code = -2; // 表示未知错误
                goto END;
            }
            if (!Compiler::Compile(file_name)) { //编译文件
                // 编译失败
                status_code = -3; // 表示代码编译时发生了错误
                goto END;
            }

            run_result = Runner::Run(file_name, cpu_limit, mem_limit);//运行可执行程序文件
            if (run_result < 0) {
                status_code = -2; // 表示未知错误
                goto END;
            } else if (run_result > 0) {
                // 程序运行崩溃了(源于某种信号)
                status_code = run_result;
            } else {
                status_code = 0;// 表示运行成功
            }
            END:
            out_value["status"] = status_code;
            out_value["reason"] = CodeToDesc(status_code, file_name);

            if (status_code == 0) {
                // 整个过程全部成功
                std::string _stdout;
                FileUtil::ReadFile(PathUtil::Stdout(file_name), &_stdout, true);
                out_value["stdout"] = _stdout;

                std::string _stderr;
                FileUtil::ReadFile(PathUtil::Stderr(file_name), &_stderr, true);
                out_value["stderr"] = _stderr;
            }

            Json::StyledWriter writer;
            *out_json = writer.write(out_value);

            RemoveTempFile(file_name);
        }
    };
}

